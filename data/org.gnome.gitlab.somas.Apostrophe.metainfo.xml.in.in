<?xml version="1.0" encoding="utf-8"?>
<component type="desktop-application">
  <id>@app-id@</id>
  <name>Apostrophe</name>
  <summary>Edit Markdown in style</summary>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0+</project_license>
  <!-- developer_name tag deprecated with Appstream 1.0 -->
  <developer_name>Manuel Genovés</developer_name>
  <developer id="es.escapist">
    <name>Manuel Genovés</name>
  </developer>
  <description>
    <p>Focus on your writing with a clean, distraction-free markdown editor.</p>
    <p>Features:</p>
    <ul>
      <li>An UI tailored to comfortable writing</li>
      <li>A distraction-free mode</li>
      <li>Dark, light and sepia themes</li>
      <li>Everything you expect from a text editor, such as spellchecking or document statistics</li>
      <li>Live preview of what you write</li>
      <li>Export to all kind of formats: PDF, Word/Libreoffice, LaTeX, or even HTML slideshows</li>
    </ul>
  </description>
  <branding>
    <color type="primary" scheme_preference="light">#90ded5</color>
    <color type="primary" scheme_preference="dark">#1b786d</color>
  </branding>
  <requires>
    <display_length compare="ge">656</display_length>
  </requires>
  <recommends>
    <display_length compare="ge">1311</display_length>
  </recommends>
  <supports>
    <control>pointing</control>
    <control>keyboard</control>
    <control>touch</control>
    <internet>offline-only</internet>
  </supports>
  <launchable type="desktop-id">@app-id@.desktop</launchable>
  <url type="homepage">https://apps.gnome.org/Apostrophe/</url>
  <url type="bugtracker">https://gitlab.gnome.org/World/apostrophe/-/issues</url>
  <url type="help">https://gitlab.gnome.org/World/apostrophe/</url>
  <url type="donation">https://www.paypal.me/manuelgenoves</url>
  <url type="vcs-browser">https://gitlab.gnome.org/World/apostrophe/</url>
  <url type="translate">https://l10n.gnome.org/module/apostrophe/</url>
  <update_contact>manuel.genoves_at_gmail.com</update_contact>
  <translation type="gettext">@gettext-package@</translation>
  <screenshots>
    <screenshot type="default">
      <image type="source">https://gitlab.gnome.org/World/apostrophe/-/raw/d50c630033213de5ee5cde0664e296b1fdcb5476/screenshots/main.png</image>
      <caption>Main window</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://gitlab.gnome.org/World/apostrophe/-/raw/d50c630033213de5ee5cde0664e296b1fdcb5476/screenshots/main-dark.png</image>
      <caption>Main window in dark mode</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://gitlab.gnome.org/World/apostrophe/-/raw/d50c630033213de5ee5cde0664e296b1fdcb5476/screenshots/table.png</image>
      <caption>Inserting a table with the help of the toolbar</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://gitlab.gnome.org/World/apostrophe/-/raw/d50c630033213de5ee5cde0664e296b1fdcb5476/screenshots/preview.png</image>
      <caption>The preview lets you see a live rendered version of your document</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://gitlab.gnome.org/World/apostrophe/-/raw/d50c630033213de5ee5cde0664e296b1fdcb5476/screenshots/focus.png</image>
      <caption>The focus mode allow for a more distraction-free experience</caption>
    </screenshot>
  </screenshots>
  <content_rating type="oars-1.1"/>
</component>
